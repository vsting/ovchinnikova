<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder {
  public function run() {
    DB::table('users')->insert([
      'name' => 'Имя Фамилия',
      'email' => 'admin@mail.com',
      'password' => bcrypt('12345678'),
      'admin' => true,
    ]);
  }
}
