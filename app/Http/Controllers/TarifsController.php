<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TarifsController extends Controller {
  public function index() {
      return view('tarifs.index');
  }

  public function minimal() {
      return view('tarifs.minimal');
  }

  public function standart() {
      return view('tarifs.standart');
  }

  public function choose() {
      return view('tarifs.choose');
  }
}
