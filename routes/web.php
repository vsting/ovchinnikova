<?php

Route::get('/', function () {
    return view('home');
});

Route::get('/welcome', function () {
    return view('welcome');
});

Route::get('/tarifs', 'TarifsController@index')->name('tarifs');
Route::get('/tarifs/{tarif}', function ($tarif) {
  if ($tarif == 1) return view('tarifs.minimal');
  if ($tarif == 2) return view('tarifs.standart');
})->name('tarifs.choose');;


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
