@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">
        <div class="card-header">Тарифы</div>

        <div class="card-body">
          <h4>Список тарифов</h4>
          <table class="table table-hover">
            <thead>
              <tr>
                <th scope="col">наименование</th>
                <th scope="col">выбор</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>«Минимальный»</td>
                <td><a class="btn btn-primary btn-sm" href="{{ route('tarifs.choose', ['tarif' => "1"]) }}" role="button">Выбрать</a></td>
              </tr>
              <tr>
                <td>«Стандартный»</td>
                <td><a class="btn btn-primary btn-sm" href="{{ route('tarifs.choose', ['tarif' => "2"]) }}" role="button">Выбрать</a></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection