@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">
        <div class="card-header"> <a href="{{ route('tarifs') }}">Тарифы</a> / Минимальный</div>

        <div class="card-body">
          Минимальный тариф
        </div>
      </div>
    </div>
  </div>
</div>
@endsection